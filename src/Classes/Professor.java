package Classes;

import java.util.Date;

public class Professor {

	private String profSex;
	private int profId;
	private String profName;
	private String profSSN;
	private double profSalary;
	private String profEmail;
	private Date startDate;
	public String getProfSex() {
		return profSex;
	}
	public void setProfSex(String profSex) {
		this.profSex = profSex;
	}
	public int getProfId() {
		return profId;
	}
	public void setProfId(int profId) {
		this.profId = profId;
	}
	public String getProfName() {
		return profName;
	}
	public void setProfName(String profName) {
		this.profName = profName;
	}
	public String getProfSSN() {
		return profSSN;
	}
	public void setProfSSN(String profSSN) {
		this.profSSN = profSSN;
	}
	public double getProfSalary() {
		return profSalary;
	}
	public void setProfSalary(double profSalary) {
		this.profSalary = profSalary;
	}
	public String getProfEmail() {
		return profEmail;
	}
	public void setProfEmail(String profEmail) {
		this.profEmail = profEmail;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	 
}
