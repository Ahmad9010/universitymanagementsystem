package Classes;

public class TeachingAssitant {
	private String taName;
	private String taSSN;
	private int taId;
	private double taSalary;
	private String taEmail;

	public String getTaName() {
		return taName;
	}

	public void setTaName(String taName) {
		this.taName = taName;
	}

	public String getTaSSN() {
		return taSSN;
	}

	public void setTaSSN(String taSSN) {
		this.taSSN = taSSN;
	}

	public int getTaId() {
		return taId;
	}

	public void setTaId(int taId) {
		this.taId = taId;
	}

	public double getTaSalary() {
		return taSalary;
	}

	public void setTaSalary(double taSalary) {
		this.taSalary = taSalary;
	}

	public String getTaEmail() {
		return taEmail;
	}

	public void setTaEmail(String taEmail) {
		this.taEmail = taEmail;
	}

}
