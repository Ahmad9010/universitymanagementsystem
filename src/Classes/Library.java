package Classes;

public class Library {
	private int libCapacity;
	private int numOfBooks;
	private int libId;

	public int getLibCapacity() {
		return libCapacity;
	}

	public void setLibCapacity(int libCapacity) {
		this.libCapacity = libCapacity;
	}

	public int getNumOfBooks() {
		return numOfBooks;
	}

	public void setNumOfBooks(int numOfBooks) {
		this.numOfBooks = numOfBooks;
	}

	public int getLibId() {
		return libId;
	}

	public void setLibId(int libId) {
		this.libId = libId;
	}

}
