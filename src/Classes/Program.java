package Classes;

public class Program {
	private int progId;
	private String progName;
	private int numOfCourse;

	public int getProgId() {
		return progId;
	}

	public void setProgId(int progId) {
		this.progId = progId;
	}

	public String getProgName() {
		return progName;
	}

	public void setProgName(String progName) {
		this.progName = progName;
	}

	public int getNumOfCourse() {
		return numOfCourse;
	}

	public void setNumOfCourse(int numOfCourse) {
		this.numOfCourse = numOfCourse;
	}

}
