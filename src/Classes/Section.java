package Classes;

public class Section {
	
	private int sectionId;
	private int numOfStudent;
	private int capacity;
	private String sectionName;
	
	public int getSectionId() {
		return sectionId;
	}
	public void setSectionId(int sectionId) {
		this.sectionId = sectionId;
	}
	public int getNumOfStudent() {
		return numOfStudent;
	}
	public void setNumOfStudent(int numOfStudent) {
		this.numOfStudent = numOfStudent;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	
	
	

}
