package Classes;

public class AcademicDepartment {

	private String deptName;
	private int deptId;
	private int noOfProgram;
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public int getDeptId() {
		return deptId;
	}
	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}
	public int getNoOfProgram() {
		return noOfProgram;
	}
	public void setNoOfProgram(int noOfProgram) {
		this.noOfProgram = noOfProgram;
	}
	
	
}
